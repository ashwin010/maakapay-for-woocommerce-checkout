<?php
/**
 * @package Maakapay_Checkout_For_Woocommerce
 * @version 1.0.0
 */

class Maakapay_For_Woocommerce_Checkout extends WC_Payment_Gateway
{

	public function __construct() {
		$this->id 					= "maakapay_for_woocommerce_checkout";
		$this->icon 				= apply_filters('woocommerce_offline_icon', plugin_dir_url( __DIR__ ) . 'assets/images/payment-logo.jpeg');
		$this->has_fields 			= false;
		$this->method_title 		= "Maakapay";
		$this->method_description 	= "Help to make Checkout easily from Woocommerce Using Maakapay Plugin";
		$this->description 			= $this->get_option( 'description' );
		$this->init_form_fields();
		$this->init_settings();
		$this->title 				= $this->get_option( 'title' );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
	}

	// Initialise Gateway Settings Form Fields
	public function init_form_fields() {
	     $this->form_fields = array(
	     	'enabled' => array(
		        'title' 	=> __( 'Enable/Disable', 'woocommerce' ),
		        'type' 		=> 'checkbox',
		        'label' 	=> __( 'Enable Maakapay For Checkout', 'woocommerce' ),
		        'default'   => 'yes'
		    ),
		     'title' => array(
		          'title' 		=> __( 'Title', 'woocommerce' ),
		          'type' 		=> 'text',
		          'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
		          'default' 	=> __( 'Maakapay', 'woocommerce' )
		    ),
		     'description' 		=> array(
		          'title' 		=> __( 'Description', 'woocommerce' ),
		          'type' 		=> 'textarea',
		          'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
		          'default' 	=> __("Enable 3D Secure™ in your card to make the purchase. The 3D Secure™ payment system is available through your bank under the name “Verified by Visa” for Visa cards or “Mastercard SecureCode” for Mastercards.", 'woocommerce')
		    ),
		    'testmode' => array(
				'title'       => 'Test mode',
				'label'       => 'Enable Test Mode',
				'type'        => 'checkbox',
				'description' => 'Place the payment gateway in test mode using test API key provided by Maakapay.',
				'default'     => 'yes',
				'desc_tip'    =>  true,
			),
			'test_api_key' => array(
				'title'       => 'Test API Key',
				'type'        => 'text',
				'description' => 'Place The Pagment Gateway Test API Key provided by Maakapay.',
				'desc_tip'    =>  true,
				'default'     => get_option('maakapay_test'),
			),
			'live_api_key' => array(
				'title'       => 'Live API Key',
				'type'        => 'text',
				'description' => 'Place The Pagment Gateway Live API Key provided by Maakapay.',
				'desc_tip'    =>  true,
				'default'     => get_option('maakapay_live'),
			)
	    );
	}

	public function payment_fields(){
	    if ( $this->description )
            if( $this->get_option( 'testmode' ) == 'yes' ) {
                $this->description .= ' <br><strong>Sandbox mode enabled</strong>';
                $this->description  = trim( $this->description );
            }
        echo wpautop( wptexturize( $this->description ) );
     }

	public function process_payment( $order_id ) {

	    global $woocommerce;

	    $order = new WC_Order( $order_id );

	    $order->update_status('pending-payment', __( 'Awaiting Card payment', 'woocommerce' ));

  	 	$response_url = $this->mpfwc_send_request_to_bank( $order );

	    return array(
				'result' 	=> 'success',
				'redirect'	=> $response_url
			);

	}

	public function mpfwc_send_request_to_bank( $order = null ) {

        global $wpdb;

        $exists = false;

        if ( !function_exists( 'wp_generate_uuid4' ) ) {

            require_once ABSPATH . WPINC . '/functions.php';
            $exists = true;

        }

        if(!is_null($order)) {

            $app_mode = ($this->get_option( 'testmode' ) == "no") ? "live" : "test";

		    $name = sanitize_text_field( $order->get_billing_first_name() ). ' ' . sanitize_text_field( $order->get_billing_last_name() );
		    $address = sanitize_text_field( $order->get_shipping_address_1() ) . ',' . sanitize_text_field( $order->get_shipping_city() ) . ',' . sanitize_text_field( $order->get_shipping_state() ) . ',' . sanitize_text_field( $order->get_shipping_postcode() ) . ',' . sanitize_text_field( $order->get_shipping_country() );
            $invoice_id = $order->id;
            $amount = $order->get_total();
            $email = $order->get_billing_email();
            $phone = $order->get_billing_phone();
            $client_ip = $this->user_ip_address();
            $currency = $order->get_currency();


            $transaction_code = ( $exists ) ? strtoupper( wp_generate_uuid4() ) : strtoupper( md5( time() ) );

            $table_name = $wpdb->prefix . "maakapay_transactions_logs";
            $data = array('name' => $name, 'invoice_number' => $invoice_id, 'transaction_code' => $transaction_code, 'amount' => $amount, 'email'=> $email, 'contact_number' => $phone, 'status' => 'pending', 'app_mode' => $app_mode, 'currency' => $currency, 'client_ip' => $client_ip, 'order_created_at' => date("Y-m-d H:i:s"));
            $format = array('%s','%d');

            $wpdb->insert( $table_name, $data, $format );

			$api_url = null;
    		$token = null;

			if( $this->get_option( 'testmode' ) == "no" ){

                $token = $this->get_option( 'live_api_key' );
                $api_url = 'https://apiapp.maakapay.com/v1/createOrder';

            }else{

                $token = $this->get_option( 'test_api_key' );
                $api_url = 'https://apisandbox.maakapay.com/v1/createOrder';

            }


			$params = [
		        'currency'         => $currency,
		        'timeout'          => 60,
		        'amount'           => $amount,
		        'name'             => $name,
		        'address'          => $address,
		        'email'            => $email,
		        'phone'            => $phone,
		        'description'      => $invoice_id,
                'transaction_code' => $transaction_code,
                'visitor_ip'       => $client_ip,
		        'approved'         => home_url( '/maakapay-success/' ),
                'canceled'         => home_url( '/maakapay-cancel/' ),
                'declined'         => home_url( '/maakapay-decline/' ),
		        'merchant_key'     => $token
		    ];

		    $response = wp_safe_remote_post( esc_url_raw( $api_url ), array(

                'body' => $params,

            ));

            if ( is_wp_error( $response ) ) {

                $error_message = $response->get_error_message();
                $data = [
                    "data" => [
                        "message" => "Something went wrong: $error_message",
                        "status" => 400
                    ]
                ];
                return json_encode( $data );

            } else {

                $body = trim( wp_remote_retrieve_body( $response ) );
                $body = json_decode($body);
                return $body->data;

            }
        }
	}

    /**
     * Get User Ip address for security purpose.
     *
     * @since    1.0.0
     */
    public function user_ip_address() {

        if( !empty( $_SERVER[ 'HTTP_CLIENT_IP' ] ) ) {
            $ip = sanitize_text_field( $_SERVER[ 'HTTP_CLIENT_IP' ] );
        } elseif( ! empty( $_SERVER[ 'HTTP_X_FORWARD_FOR' ] ) ) {
            $ip = sanitize_text_field( $_SERVER[ 'HTTP_X_FORWARD_FOR' ] );
        } else {
            $ip = sanitize_text_field( $_SERVER[ 'REMOTE_ADDR' ] );
        }

        return sanitize_text_field( $ip );
    }

}
